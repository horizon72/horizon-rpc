package com.horizon72.rpc.server.config;

import java.util.HashMap;
import java.util.Map;

public final class ServerCache {
    private static final Map<String, Object> serverCacheMap = new HashMap<>();

    public static void store(String serverName, Object server) {
        serverCacheMap.merge(serverName, server, (Object oldObj, Object newObj) -> newObj);
    }

    public static Object get(String serverName) {
        return serverCacheMap.get(serverName);
    }

    public static Map<String, Object> getAll() {
        return null;
    }
}