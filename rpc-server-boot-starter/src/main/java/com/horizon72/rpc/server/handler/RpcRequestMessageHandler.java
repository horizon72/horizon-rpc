package com.horizon72.rpc.server.handler;

import com.horizon72.rpc.message.RpcRequestMessage;
import com.horizon72.rpc.message.RpcResponseMessage;
import com.horizon72.rpc.server.config.ServerCache;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Method;

@Slf4j
public class RpcRequestMessageHandler extends SimpleChannelInboundHandler<RpcRequestMessage> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, RpcRequestMessage message) {
        RpcResponseMessage rpcResponseMessage = new RpcResponseMessage();
        rpcResponseMessage.setSequenceId(message.getSequenceId());
        try {
            String serviceName = message.getInterfaceName();
            Object service = ServerCache.get(serviceName);
            log.info("got service {}",service);
            if(service==null){
                throw new RuntimeException("service not found in rpc-server");
            }
            Method method = service.getClass()
                    .getMethod(message.getMethodName(), message.getParameterTypes());
            Object invoke = method.invoke(service, message.getParameterValue());
            rpcResponseMessage.setReturnValue(invoke);
        } catch (Exception e) {
            log.error(e.getMessage());
            rpcResponseMessage.setReturnValue(new RuntimeException("远程调用出错" + e.getMessage()));
        }
        ctx.writeAndFlush(rpcResponseMessage);
    }
}
