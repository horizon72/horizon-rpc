package com.horizon72.rpc.server.service;

import com.horizon72.rpc.common.ServiceInfo;

public interface RegisterService {
    void register(ServiceInfo serviceInfo);

    void destroy();

}
