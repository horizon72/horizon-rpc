package com.horizon72.rpc.server.config;

import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.client.naming.NacosNamingService;
import com.horizon72.rpc.server.annotation.RpcServicePostProcessor;
import com.horizon72.rpc.server.common.RpcServerProperties;
import com.horizon72.rpc.server.runner.NettyRpcServer;
import com.horizon72.rpc.server.runner.RpcServer;
import com.horizon72.rpc.server.service.NacosRegisterService;
import com.horizon72.rpc.server.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.context.properties.bind.BindResult;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class RpcServerAutoConfiguration {

    @Bean
    public RpcServerProperties rpcServerProperties(Environment environment){
        BindResult<RpcServerProperties> bindResult = Binder.get(environment)
                .bind("rpc.server", RpcServerProperties.class);
        return bindResult.get();
    }

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnBean({RpcServerProperties.class})
    public NamingService namingService(RpcServerProperties properties){
        return new NacosNamingService(properties.getRegistryAddr());
    }
    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnBean({NamingService.class})
    public RegisterService registerService(NamingService namingService) {
        return new NacosRegisterService(namingService);
    }

    @Bean
    @ConditionalOnMissingBean
    public RpcServer getRpcServer(){return new NettyRpcServer();};

    @Bean
    public RpcServicePostProcessor rpcServicePostProcessor(){return new RpcServicePostProcessor();}
}
