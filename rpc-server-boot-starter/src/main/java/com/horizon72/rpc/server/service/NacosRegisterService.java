package com.horizon72.rpc.server.service;

import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingService;
import com.horizon72.rpc.common.ServiceInfo;
import lombok.extern.slf4j.Slf4j;

import static com.horizon72.rpc.common.RpcConst.DEFAULT_GROUP_NAME;

@Slf4j
public class NacosRegisterService implements RegisterService {

    private NamingService namingService;



    public NacosRegisterService(NamingService namingService) {
        this.namingService = namingService;
    }

    @Override
    public void register(ServiceInfo serviceInfo) {
        String serviceName = serviceInfo.getServiceName();
        String serviceAddress = serviceInfo.getServiceAddress();
        Integer port = serviceInfo.getPort();
        try {
            namingService.registerInstance(serviceName, DEFAULT_GROUP_NAME,
                    serviceAddress, port);
        } catch (NacosException e) {
            log.error(e.getErrMsg());
            throw new RuntimeException("服务注册失败" + e.getErrMsg());
        }
    }


    @Override
    public void destroy() {

    }

}
