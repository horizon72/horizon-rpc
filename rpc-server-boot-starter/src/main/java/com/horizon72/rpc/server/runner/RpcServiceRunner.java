package com.horizon72.rpc.server.runner;

import com.horizon72.rpc.coder.Serializer;
import com.horizon72.rpc.server.common.RpcServerProperties;
import com.horizon72.rpc.server.service.RegisterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class RpcServiceRunner implements CommandLineRunner {
    private RpcServer rpcServer;
    private RpcServerProperties rpcServerProperties;
    private RegisterService registerService;

    private Serializer serializer;
    public RpcServiceRunner(RpcServer rpcServer, RpcServerProperties rpcServerProperties,
                            RegisterService registerService, Serializer serializer) {
        this.rpcServer = rpcServer;
        this.rpcServerProperties = rpcServerProperties;
        this.registerService = registerService;
        this.serializer=serializer;
    }

    @Override
    public void run(String... args) {
        new Thread(() -> rpcServer.start(rpcServerProperties.getPort(),serializer)).start();
        log.info(" rpc server :{} start, appName :{} , port :{}", rpcServer, rpcServerProperties.getAppName()
                , rpcServerProperties.getPort());
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                // 关闭之后把服务清除
                registerService.destroy();
            } catch (Exception ex) {
                log.error("", ex);
            }

        }));
    }
}
