package com.horizon72.rpc.server.runner;

import com.horizon72.rpc.coder.Serializer;

public interface RpcServer {
    void start(int port, Serializer serializer);
}
