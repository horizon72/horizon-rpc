package com.horizon72.rpc.server.runner;

import com.horizon72.rpc.coder.MessageCoder;
import com.horizon72.rpc.coder.ProcotolFrameDecoder;
import com.horizon72.rpc.coder.Serializer;
import com.horizon72.rpc.server.handler.RpcRequestMessageHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import lombok.extern.slf4j.Slf4j;

import java.net.InetAddress;

@Slf4j
public class NettyRpcServer implements RpcServer {

    @Override
    public void start(int port, Serializer serializer) {
        NioEventLoopGroup boss = new NioEventLoopGroup();
        NioEventLoopGroup worker = new NioEventLoopGroup();
        try {
            String serverAddress = InetAddress.getLocalHost().getHostAddress();
            ServerBootstrap bootstrap = new ServerBootstrap();
            bootstrap.group(boss, worker).channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>(){
                        @Override
                        protected void initChannel(SocketChannel ch) {
                            ProcotolFrameDecoder FRAME_DECODER = new ProcotolFrameDecoder();
                            LoggingHandler LOGGING_HANDLER = new LoggingHandler(LogLevel.INFO);
                            RpcRequestMessageHandler RPC_HANDLER = new RpcRequestMessageHandler();
                            MessageCoder messageCoder=new MessageCoder(serializer);
                            ch.pipeline().addLast(FRAME_DECODER);
                            ch.pipeline().addLast(LOGGING_HANDLER);
                            ch.pipeline().addLast(messageCoder);
                            ch.pipeline().addLast(RPC_HANDLER);
                        }
                    });
            ChannelFuture channelFuture = bootstrap.bind(serverAddress, port).sync();
            log.info("server addr {} started on port {}", serverAddress, port);
            channelFuture.channel().closeFuture().sync();
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            boss.shutdownGracefully();
            worker.shutdownGracefully();
        }
    }
}
