package com.horizon72.rpc.server.common;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
public class RpcServerProperties {


    /**
     *  服务启动端口
     */
    private Integer port;

    /**
     *  服务名称
     */
    private String appName;

    /**
     *  注册中心地址
     */
    private String registryAddr = "127.0.0.1:8848";


}
