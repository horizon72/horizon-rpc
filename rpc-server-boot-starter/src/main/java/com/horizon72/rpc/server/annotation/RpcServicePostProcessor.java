package com.horizon72.rpc.server.annotation;


import com.horizon72.rpc.common.ServiceInfo;
import com.horizon72.rpc.server.common.RpcServerProperties;
import com.horizon72.rpc.server.config.ServerCache;
import com.horizon72.rpc.server.service.RegisterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;

import java.net.InetAddress;

@Slf4j
public class RpcServicePostProcessor implements BeanPostProcessor {

    @Autowired
    RpcServerProperties properties;

    @Autowired
    RegisterService registerService;

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) {
        try {
            RpcService rpcService = bean.getClass().getAnnotation(RpcService.class);
            if (rpcService != null) {
                String serviceName = rpcService.serviceType().getName();
                Integer port = properties.getPort();
                ServiceInfo serviceInfo = ServiceInfo.builder()
                        .serviceName(serviceName).serviceAddress(InetAddress.getLocalHost().getHostAddress())
                        .port(port).build();
                ServerCache.store(serviceName, bean);
                registerService.register(serviceInfo);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return bean;
    }
}
