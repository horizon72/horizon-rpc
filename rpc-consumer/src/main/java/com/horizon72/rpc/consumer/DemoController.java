package com.horizon72.rpc.consumer;

import com.horizon72.rpc.api.service.UserService;
import com.horizon72.rpc.client.annotation.RpcAutowired;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/demo")
public class DemoController {

    @RpcAutowired
    private UserService userService;

    @GetMapping("/get")
    public String useService(@RequestParam int userId) {
        return userService.getUser(userId);
    }
}
