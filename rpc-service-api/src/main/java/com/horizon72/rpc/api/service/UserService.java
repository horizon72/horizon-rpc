package com.horizon72.rpc.api.service;

public interface UserService {
    String getUser(int userId);
}
