package com.horizon72.rpc.service;

import com.horizon72.rpc.api.service.UserService;
import com.horizon72.rpc.server.annotation.RpcService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RpcService(serviceType = UserService.class)
public class UserServiceImpl implements UserService {
    @Override
    public String getUser(int userId) {
        return userId == 1 ? "one" : "else";
    }
}
