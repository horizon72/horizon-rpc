package rpc.common.service;

import lombok.Data;

@Data
public class ServiceInfo {
   private String serviceName;
   private Integer port;
   private String registryAddr;
}
