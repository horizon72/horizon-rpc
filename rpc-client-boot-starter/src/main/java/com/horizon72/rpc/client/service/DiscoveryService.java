package com.horizon72.rpc.client.service;

import com.alibaba.nacos.api.naming.pojo.Instance;

public interface DiscoveryService {
    Instance lookup(String serviceName);
}
