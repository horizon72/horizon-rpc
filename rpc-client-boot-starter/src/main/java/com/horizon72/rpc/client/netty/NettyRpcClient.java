package com.horizon72.rpc.client.netty;

import com.horizon72.rpc.client.cache.PromiseCache;
import com.horizon72.rpc.client.netty.handler.RpcResponseMessageHandler;
import com.horizon72.rpc.coder.MessageCoder;
import com.horizon72.rpc.coder.ProcotolFrameDecoder;
import com.horizon72.rpc.coder.Serializer;
import com.horizon72.rpc.common.ServiceInfo;
import com.horizon72.rpc.message.RpcRequestMessage;
import com.horizon72.rpc.message.RpcResponseMessage;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.util.concurrent.DefaultPromise;
import io.netty.util.concurrent.Promise;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.concurrent.TimeUnit;

@Slf4j
public class NettyRpcClient implements RpcClient, ApplicationContextAware {

    private Bootstrap bootstrap;

    private NioEventLoopGroup group;

    public NettyRpcClient(Serializer serializer) {
        ProcotolFrameDecoder FRAME_DECODER = new ProcotolFrameDecoder();
        LoggingHandler LOGGING_HANDLER = new LoggingHandler(LogLevel.DEBUG);
        RpcResponseMessageHandler rpcResponseMessageHandler = new RpcResponseMessageHandler();
        bootstrap = new Bootstrap();
        group = new NioEventLoopGroup(4);
        bootstrap.group(group).channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) {
                        MessageCoder messageCoder = new MessageCoder(serializer);
                        ch.pipeline().addLast(LOGGING_HANDLER);
                        ch.pipeline().addLast(FRAME_DECODER);
                        ch.pipeline().addLast(messageCoder);
                        ch.pipeline().addLast(rpcResponseMessageHandler);
                    }
                });
    }


    @Override
    public RpcResponseMessage sendRequest(ServiceInfo serviceInfo
            , RpcRequestMessage requestMessage) throws Exception {

        ChannelFuture channelFuture = bootstrap
                .connect(serviceInfo.getServiceAddress(), serviceInfo.getPort()).sync();
        channelFuture.addListener((ChannelFutureListener) future -> {
            if (channelFuture.isSuccess()) {
                log.info("channel connect success {}", channelFuture);
            } else {
                log.error("channel connect fail {}", channelFuture);
                channelFuture.cause().printStackTrace();
                group.shutdownGracefully();
            }
        });
        Channel channel = channelFuture.sync().channel();
        Promise<Object> promise = new DefaultPromise<>(channel.eventLoop());
        PromiseCache.add(requestMessage.getSequenceId(), promise);
        channel.writeAndFlush(requestMessage);
        Object o = promise.get(requestMessage.getTimeout(), TimeUnit.SECONDS);
        return (RpcResponseMessage) o;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {

    }
}
