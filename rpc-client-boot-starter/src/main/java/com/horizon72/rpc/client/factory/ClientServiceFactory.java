package com.horizon72.rpc.client.factory;

import com.horizon72.rpc.client.common.RpcClientProperties;
import com.horizon72.rpc.client.proxy.ProxyServiceInvocationHandler;
import com.horizon72.rpc.client.service.DiscoveryService;
import com.horizon72.rpc.coder.Serializer;
import org.springframework.stereotype.Component;

import java.lang.reflect.Proxy;
import java.util.WeakHashMap;

@Component
public class ClientServiceFactory {
    private WeakHashMap<Class<?>, Object> beanProxyMap = new WeakHashMap<>();

    public <T> T getProxy(Class<T> clazz, DiscoveryService discoveryService,
                          RpcClientProperties properties, Serializer serializer) {
        return (T) beanProxyMap.computeIfAbsent(clazz, clz -> Proxy.newProxyInstance(clz.getClassLoader()
                , new Class[]{clz}, new ProxyServiceInvocationHandler(clz, discoveryService,
                        properties,serializer)));
    }
}
