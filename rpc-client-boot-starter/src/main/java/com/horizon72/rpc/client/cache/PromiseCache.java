package com.horizon72.rpc.client.cache;

import com.horizon72.rpc.message.RpcResponseMessage;
import io.netty.util.concurrent.Promise;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class PromiseCache {
    private static Map<Integer, Promise<Object>> PROMISE_MAP=new ConcurrentHashMap();
    public static void add(int sequenceId, Promise<Object> promise){
        PROMISE_MAP.put(sequenceId,promise);
    }
    public static void fillResponse(int sequenceId, RpcResponseMessage message){
        Promise<Object> promise = PROMISE_MAP.remove(sequenceId);
        if (promise != null) {
            promise.setSuccess(message);
        }
    }
}
