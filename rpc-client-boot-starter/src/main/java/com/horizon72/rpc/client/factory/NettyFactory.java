package com.horizon72.rpc.client.factory;

import com.horizon72.rpc.client.netty.NettyRpcClient;
import com.horizon72.rpc.client.netty.RpcClient;
import com.horizon72.rpc.coder.MessageCoder;
import com.horizon72.rpc.coder.Serializer;
import org.springframework.stereotype.Component;

@Component
public class NettyFactory {

    private MessageCoder messageCoder;


    public NettyFactory(MessageCoder messageCoder) {
        this.messageCoder = messageCoder;
    }

    public static RpcClient buildRpcClient(Serializer serializer) {
        return new NettyRpcClient(serializer);
    }
}
