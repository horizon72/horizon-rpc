package com.horizon72.rpc.client.proxy;

import com.alibaba.nacos.api.naming.pojo.Instance;
import com.horizon72.rpc.client.common.RpcClientProperties;
import com.horizon72.rpc.client.factory.NettyFactory;
import com.horizon72.rpc.client.service.DiscoveryService;
import com.horizon72.rpc.coder.Serializer;
import com.horizon72.rpc.common.ServiceInfo;
import com.horizon72.rpc.common.ServiceUtils;
import com.horizon72.rpc.message.RpcRequestMessage;
import com.horizon72.rpc.message.RpcResponseMessage;
import com.horizon72.rpc.uuid.SequenceIdGenerator;
import com.horizon72.rpc.uuid.UuidUtils;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

@Slf4j
public class ProxyServiceInvocationHandler implements InvocationHandler {

    private Class<?> clazz;

    private DiscoveryService discoveryService;

    private RpcClientProperties properties;
    private Serializer serializer;

    public <T> ProxyServiceInvocationHandler(Class<?> clazz, DiscoveryService discoveryService
            , RpcClientProperties properties, Serializer serializer) {
        this.clazz = clazz;
        this.discoveryService = discoveryService;
        this.properties = properties;
        this.serializer=serializer;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) {
        try {
            Instance instance = discoveryService.lookup(clazz.getName());
            if(instance==null){
                throw new RuntimeException("cannot find service "+clazz.getName());
            }
            RpcRequestMessage requestMessage =new RpcRequestMessage();
            String serviceName = ServiceUtils.
                    getServiceNameFromNaocsInstance(instance.getServiceName());
            requestMessage.setMessageType(requestMessage.getMessageType());
            requestMessage.setSequenceId(UuidUtils.getUuid());
            requestMessage.setInterfaceName(serviceName);
            requestMessage.setMethodName(method.getName());
            requestMessage.setReturnType(method.getReturnType());
            requestMessage.setParameterValue(args);
            requestMessage.setParameterTypes(method.getParameterTypes());
            ServiceInfo serviceInfo = ServiceInfo.builder().serviceName(serviceName)
                    .serviceAddress(instance.getIp()).port(instance.getPort()).build();
            RpcResponseMessage rpcResponseMessage = NettyFactory.buildRpcClient(serializer)
                    .sendRequest(serviceInfo, requestMessage);
            if (rpcResponseMessage == null) {
                log.error("请求超时");
                throw new RuntimeException("rpc调用结果失败， 请求超时 timeout:" + properties.getTimeout());
            }
            Exception exceptionValue = rpcResponseMessage.getExceptionValue();
            if (exceptionValue != null) {
                log.error("rpc调用失败，出现异常{}", exceptionValue.getMessage());
                throw new RuntimeException(exceptionValue.getMessage());
            }
            Object returnValue = rpcResponseMessage.getReturnValue();
            return returnValue;
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
            return null;
        }
    }
}
