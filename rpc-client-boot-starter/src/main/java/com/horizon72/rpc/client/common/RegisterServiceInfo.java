package com.horizon72.rpc.client.common;

import lombok.Data;

@Data
public class RegisterServiceInfo {
    private String serviceName;
    private String ipAddress;
    private String port;
}
