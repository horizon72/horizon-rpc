package com.horizon72.rpc.client.config;

import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.client.naming.NacosNamingService;
import com.horizon72.rpc.client.annotation.RpcAutowiredBeanFactoryPostProcessor;
import com.horizon72.rpc.client.common.RpcClientProperties;
import com.horizon72.rpc.client.factory.ClientServiceFactory;
import com.horizon72.rpc.client.service.DiscoveryService;
import com.horizon72.rpc.client.service.NacosDiscoveryService;
import com.horizon72.rpc.coder.JavaSerializer;
import com.horizon72.rpc.coder.Serializer;
import com.horizon72.rpc.loadbalance.LoadBalance;
import com.horizon72.rpc.loadbalance.RoundRobinLoadBalance;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.bind.BindResult;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class RpcClientAutoConfiguration {

    @Bean
    public RpcClientProperties rpcClientProperties(Environment environment) {
        BindResult<RpcClientProperties> result = Binder.get(environment).bind("rpc.client", RpcClientProperties.class);
        return result.get();
    }

    @Bean
    @ConditionalOnMissingBean
    public LoadBalance loadBalance() {
        return new RoundRobinLoadBalance();
    }

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnBean({RpcClientProperties.class})
    public NamingService namingService(RpcClientProperties properties) {
        return new NacosNamingService(properties.getDiscoveryAddr());
    }

    @Bean
    @ConditionalOnMissingBean
    @ConditionalOnBean({NamingService.class, LoadBalance.class})
    public DiscoveryService discoveryService(NamingService namingService
            , LoadBalance loadBalance) {
        return new NacosDiscoveryService(namingService, loadBalance);
    }

    @Bean
    @ConditionalOnMissingBean
    public Serializer serializer(){
        return new JavaSerializer();
    }

    @Bean
    @ConditionalOnMissingBean
    public RpcAutowiredBeanFactoryPostProcessor rpcAutowiredBeanFactoryPostProcessor
            (ClientServiceFactory clientServiceFactory,
             DiscoveryService discoveryService, RpcClientProperties properties,
             Serializer serializer) {
        return new RpcAutowiredBeanFactoryPostProcessor(clientServiceFactory, discoveryService,
                properties,serializer);
    }


}
