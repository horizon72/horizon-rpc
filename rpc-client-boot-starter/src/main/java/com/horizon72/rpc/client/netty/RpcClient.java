package com.horizon72.rpc.client.netty;

import com.horizon72.rpc.message.RpcRequestMessage;
import com.horizon72.rpc.message.RpcResponseMessage;
import com.horizon72.rpc.common.ServiceInfo;


public interface RpcClient {
    RpcResponseMessage sendRequest(ServiceInfo serviceInfo
            , RpcRequestMessage rpcRequestMessage) throws Exception;
}
