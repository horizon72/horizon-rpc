package com.horizon72.rpc.client.annotation;

import com.horizon72.rpc.client.common.RpcClientProperties;
import com.horizon72.rpc.client.factory.ClientServiceFactory;
import com.horizon72.rpc.client.service.DiscoveryService;
import com.horizon72.rpc.coder.Serializer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.ClassUtils;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

@Slf4j
public class RpcAutowiredBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

    private ClientServiceFactory clientServiceFactory;

    private DiscoveryService discoveryService;
    private RpcClientProperties properties;
    private Serializer serializer;

    public RpcAutowiredBeanFactoryPostProcessor(ClientServiceFactory clientServiceFactory,
                                                DiscoveryService discoveryService,
                                                RpcClientProperties properties,
                                                Serializer serializer) {
        this.clientServiceFactory = clientServiceFactory;
        this.discoveryService = discoveryService;
        this.properties = properties;
        this.serializer=serializer;
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory
                                               beanFactory) {
        String[] beanDefinitionNames = beanFactory.getBeanDefinitionNames();
        for (String beanDefinitionName : beanDefinitionNames) {
            BeanDefinition beanDefinition = beanFactory.getBeanDefinition(beanDefinitionName);
            String beanClassName = beanDefinition.getBeanClassName();
            if (!StringUtils.isEmpty(beanClassName)) {
                Class<?> clazz = ClassUtils.resolveClassName(beanClassName, this.getClass().getClassLoader());
                ReflectionUtils.doWithFields(clazz, field -> {
                    RpcAutowired rpcAutowired = AnnotationUtils.getAnnotation(field, RpcAutowired.class);
                    if (rpcAutowired != null) {
                        Object bean = beanFactory.getBean(clazz);
                        field.setAccessible(Boolean.TRUE);
                        log.debug("bean is {}, and field is {}", bean, field);
                        ReflectionUtils.setField(field, bean, clientServiceFactory
                                .getProxy(field.getType(), discoveryService, properties,serializer));
                    }
                });
            }
        }
    }
}
