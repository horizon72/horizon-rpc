package com.horizon72.rpc.client.netty.handler;

import com.horizon72.rpc.client.cache.PromiseCache;
import com.horizon72.rpc.message.RpcResponseMessage;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.concurrent.Promise;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
public class RpcResponseMessageHandler extends SimpleChannelInboundHandler<RpcResponseMessage> {

    public static final Map<Integer, Promise<Object>> PROMISE_MAP = new ConcurrentHashMap<>();

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, RpcResponseMessage msg) {
        log.debug("{}", msg);
//        Promise<Object> promise = PROMISE_MAP.remove(msg.getSequenceId());
//        if (promise != null) {
//            Object returnValue = msg.getReturnValue();
//            Exception exceptionValue = msg.getExceptionValue();
//            if (exceptionValue != null) {
//                promise.setFailure(exceptionValue);
//            } else {
//                promise.setSuccess(returnValue);
//            }
//        }
        PromiseCache.fillResponse(msg.getSequenceId(), msg);
    }
}
