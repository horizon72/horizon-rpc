package com.horizon72.rpc.client.service;

import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.pojo.Instance;
import com.horizon72.rpc.loadbalance.LoadBalance;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

import static com.horizon72.rpc.common.RpcConst.DEFAULT_GROUP_NAME;

@Slf4j
public class NacosDiscoveryService implements DiscoveryService {

    public NacosDiscoveryService(NamingService namingService,
                                 LoadBalance loadBalance) {
        this.namingService = namingService;
        this.loadBalance = loadBalance;
    }

    private NamingService namingService;


    private LoadBalance loadBalance;

    @Override
    public Instance lookup(String serviceName) {
        try {
            List<Instance> allInstances = namingService.getAllInstances(serviceName,
                    DEFAULT_GROUP_NAME);
            if (allInstances.isEmpty()) {
                throw new RuntimeException("未找到该服务" + serviceName);
            }
            Instance instance = loadBalance.getOne(allInstances);
            return instance;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new RuntimeException("服务发现失败" + e.getMessage());
        }
    }
}
