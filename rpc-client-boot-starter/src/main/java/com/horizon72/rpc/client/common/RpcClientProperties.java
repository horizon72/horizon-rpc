package com.horizon72.rpc.client.common;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
public class RpcClientProperties {
    /**
     * 负载均衡
     */
    private String balance;

    /**
     * 序列化
     */
    private String serialization;

    /**
     * 服务发现地址
     */
    private String discoveryAddr = "127.0.0.1:8848";

    /**
     * 服务调用超时
     */
    private Integer timeout;
}
