package com.horizon72.rpc.coder;

public interface Serializer {

    byte getNum();

    <T> T deserialize(Class<T> clazz, byte[] bytes);
    // 序列化方法
    <T> byte[] serialize(T object);
}
