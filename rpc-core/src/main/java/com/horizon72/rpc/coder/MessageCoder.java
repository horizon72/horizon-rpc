package com.horizon72.rpc.coder;


import com.horizon72.rpc.message.BaseMessage;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageCodec;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MessageCoder extends ByteToMessageCodec<BaseMessage> {


    private Serializer serializer;

    public MessageCoder(Serializer serializer) {
        this.serializer = serializer;
    }

    @Override
    protected void encode(ChannelHandlerContext ctx, BaseMessage msg, ByteBuf out) throws Exception {
        // 1. 4字节的魔数
        out.writeBytes(new byte[]{1,2,3,4});
        // 2. 1字节的版本
        out.writeByte(1);
        // 3. 1字节的序列化方式 jdk 0 json 1
        out.writeByte(serializer.getNum());
        // 4. 1字节的指令类型
        out.writeByte(msg.getMessageType());
        // 5. 4个字节
        out.writeInt(msg.getSequenceId());
        // 对齐
        out.writeByte(0xff);
        // 6. 获取内容的字节数组
        byte[] bytes = serializer.serialize(msg);
        // 7. 长度
        out.writeInt(bytes.length);
        // 8. 写入内容
        out.writeBytes(bytes);
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        int magicNum = in.readInt();
        byte version = in.readByte();
        byte serializerAlgorithm = in.readByte();
        byte messageType = in.readByte();
        int sequenceId = in.readInt();
        in.readByte();
        int length = in.readInt();
        byte[] bytes = new byte[length];
        in.readBytes(bytes, 0, length);
        Class<? extends BaseMessage> messageClass = BaseMessage.getMessageClass(messageType);
        Object message = serializer.deserialize(messageClass, bytes);
        out.add(message);
    }
}
