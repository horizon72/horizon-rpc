package com.horizon72.rpc.coder;

import org.springframework.stereotype.Component;

import java.io.*;

@Component
public class JavaSerializer implements Serializer{
    @Override
    public byte getNum() {
        return 0;
    }

    @Override
    public <T> T deserialize(Class<T> clazz, byte[] bytes) {
        try {
            ObjectInputStream objectInputStream=new ObjectInputStream(new ByteArrayInputStream(bytes));
            return (T) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("反序列化失败", e);
        }
    }

    @Override
    public <T> byte[] serialize(T object) {
        try {
            ByteArrayOutputStream byteArrayOutputStream=new ByteArrayOutputStream();
            ObjectOutputStream outputStream=new ObjectOutputStream(byteArrayOutputStream);
            outputStream.writeObject(object);
            return byteArrayOutputStream.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("序列化失败", e);
        }
    }
}
