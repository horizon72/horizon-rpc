package com.horizon72.rpc.loadbalance;

import com.alibaba.nacos.api.naming.pojo.Instance;
import com.horizon72.rpc.common.ServiceInfo;

import java.util.List;

public interface LoadBalance {
    Instance getOne(List<Instance> serviceInfos);
}
