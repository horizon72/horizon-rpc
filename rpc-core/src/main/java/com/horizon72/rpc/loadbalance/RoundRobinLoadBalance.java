package com.horizon72.rpc.loadbalance;


import com.alibaba.nacos.api.naming.pojo.Instance;
import com.horizon72.rpc.common.ServiceInfo;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class RoundRobinLoadBalance implements LoadBalance {
    private AtomicInteger atomicInteger = new AtomicInteger();

    @Override
    public Instance getOne(List<Instance> serviceInfos) {
        int index = atomicInteger.incrementAndGet() % serviceInfos.size();
        return serviceInfos.get(index);
    }

}
