package com.horizon72.rpc.common;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ServiceInfo {
    private String serviceName;
    private Integer port;
    private String serviceAddress;
}
