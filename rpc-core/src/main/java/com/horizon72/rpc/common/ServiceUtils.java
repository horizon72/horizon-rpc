package com.horizon72.rpc.common;

import org.springframework.util.StringUtils;

public class ServiceUtils {
    public static String getServiceNameFromNaocsInstance(String gServiceName) {
        String temp = gServiceName;
        String groupName = temp.split("@@", -1)[0];
        String serviceName = gServiceName.replaceFirst(groupName+"@@", "");
        if (StringUtils.isEmpty(groupName) || StringUtils.isEmpty(serviceName)) {
            throw new RuntimeException("groupName or serviceName cannot be empty");
        }
        return serviceName;
    }
}
