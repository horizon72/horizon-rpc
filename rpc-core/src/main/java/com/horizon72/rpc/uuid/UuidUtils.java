package com.horizon72.rpc.uuid;

import java.util.UUID;

public class UuidUtils {
    public static final Integer getUuid(){
        int code = UUID.randomUUID().toString()
                .replaceAll("-", "").hashCode();
        return code;
    }
}
